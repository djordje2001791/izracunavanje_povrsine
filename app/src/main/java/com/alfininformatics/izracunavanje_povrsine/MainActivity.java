package com.alfininformatics.izracunavanje_povrsine;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    RadioGroup shape_group;

    RadioButton shape_square,
                shape_rectangle,
                shape_triangle,
                shape_circle,
                shape_ellipse;

    LinearLayout first_num,
                 second_num,
                 third_num,
                 result;

    TextView first_num_label,
             second_num_label,
             third_num_label,
             result_label;

    EditText first_num_input,
             second_num_input,
             third_num_input,
             result_input;

    Button calculate_button;

    int selectedShape;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        shape_group = (RadioGroup)findViewById(R.id.shape_group);

        shape_square = (RadioButton)findViewById(R.id.shape_square);
        shape_rectangle = (RadioButton)findViewById(R.id.shape_rectangle);
        shape_triangle = (RadioButton)findViewById(R.id.shape_triangle);
        shape_circle = (RadioButton)findViewById(R.id.shape_circle);
        shape_ellipse = (RadioButton)findViewById(R.id.shape_ellipse);

        first_num = (LinearLayout)findViewById(R.id.first_num);
        second_num = (LinearLayout)findViewById(R.id.second_num);
        third_num = (LinearLayout)findViewById(R.id.third_num);
        result = (LinearLayout)findViewById(R.id.result);

        first_num_label = (TextView)findViewById(R.id.first_num_label);
        second_num_label = (TextView)findViewById(R.id.second_num_label);
        third_num_label = (TextView)findViewById(R.id.third_num_label);
        result_label = (TextView)findViewById(R.id.result_label);

        first_num_input = (EditText)findViewById(R.id.first_num_input);
        second_num_input = (EditText)findViewById(R.id.second_num_input);
        third_num_input = (EditText)findViewById(R.id.third_num_input);
        result_input = (EditText)findViewById(R.id.result_input);

        calculate_button = (Button)findViewById(R.id.calculate_button);

        RadioButton checkedRadioButton = (RadioButton)shape_group.findViewById(shape_group.getCheckedRadioButtonId());
        shape_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.shape_square:
                        first_num.setVisibility(View.VISIBLE);
                        second_num.setVisibility(View.INVISIBLE);
                        third_num.setVisibility(View.INVISIBLE);

                        first_num_label.setText("Дужина странице: ");

                        selectedShape = 0;

                        break;
                    case R.id.shape_rectangle:
                        first_num.setVisibility(View.VISIBLE);
                        second_num.setVisibility(View.VISIBLE);
                        third_num.setVisibility(View.INVISIBLE);

                        first_num_label.setText("Дужина једне странице: ");
                        second_num_label.setText("Дужина друге странице: ");

                        selectedShape = 1;

                        break;
                    case R.id.shape_triangle:
                        first_num.setVisibility(View.VISIBLE);
                        second_num.setVisibility(View.VISIBLE);
                        third_num.setVisibility(View.VISIBLE);

                        first_num_label.setText("Дужина једне странице: ");
                        second_num_label.setText("Дужина друге странице: ");
                        third_num_label.setText("Дужина друге странице: ");

                        selectedShape = 2;

                        break;
                    case R.id.shape_circle:
                        first_num.setVisibility(View.VISIBLE);
                        second_num.setVisibility(View.INVISIBLE);
                        third_num.setVisibility(View.INVISIBLE);

                        first_num_label.setText("Дужина полупречника: ");

                        selectedShape = 3;

                        break;
                    case R.id.shape_ellipse:
                        first_num.setVisibility(View.VISIBLE);
                        second_num.setVisibility(View.VISIBLE);
                        third_num.setVisibility(View.INVISIBLE);

                        first_num_label.setText("Дужина краћег полупречника: ");
                        second_num_label.setText("Дужина дужег полупречника: ");

                        selectedShape = 4;

                        break;
                }
            }
        });

        calculate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(first_num_input.length() == 0)
                    first_num_input.setText("0");
                if(second_num_input.length() == 0)
                    second_num_input.setText("0");
                if(third_num_input.length() == 0)
                    third_num_input.setText("0");

                double firstNum = Double.parseDouble(first_num_input.getText().toString()),
                       secondNum = Double.parseDouble(second_num_input.getText().toString()),
                       thirdNum = Double.parseDouble(third_num_input.getText().toString()),
                       result = 0;

                switch(selectedShape) {
                    case 0:
                        result = Math.pow(firstNum, 2);

                        break;
                    case 1:
                        result = firstNum * secondNum;

                        break;
                    case 2:
                        double half_perimeter = (firstNum + secondNum + thirdNum) / 2;
                        result = Math.sqrt(half_perimeter * (half_perimeter - firstNum) * (half_perimeter - secondNum) * (half_perimeter - thirdNum));

                        break;
                    case 3:
                        result = Math.pow(firstNum, 2) * Math.PI;

                        break;
                    case 4:
                        result = firstNum * secondNum * Math.PI;

                        break;
                }

                result_input.setText(String.valueOf(result));
            }
        });

    }


}
